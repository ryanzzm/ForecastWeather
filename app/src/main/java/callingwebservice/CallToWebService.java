package callingwebservice;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.AbstractHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.net.MalformedURLException;
import java.security.KeyStore;
import java.util.ArrayList;

import interfaces.CallBack;

/**
 * Created by 583583 on 04-07-2016.
 */
public class CallToWebService extends
        AsyncTask<String, Void, ArrayList<String>> {
    CallBack callBackListener;
    private Context localContext;
    private String msg;
    private ProgressDialog progDialog;

    public CallToWebService(CallBack listener, Context context, String message) {
        callBackListener = listener;
        localContext = context;
        msg = message;
    }

    public static DefaultHttpClient getNewHttpClient() {
        try {
            KeyStore trustStore = KeyStore.getInstance(KeyStore
                    .getDefaultType());
            trustStore.load(null, null);

            MySSLSocketFactory sf = new MySSLSocketFactory(trustStore);
            sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

            HttpParams params = new BasicHttpParams();
            HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
            HttpProtocolParams.setContentCharset(params, HTTP.UTF_8);

            SchemeRegistry registry = new SchemeRegistry();
            registry.register(new Scheme("http", PlainSocketFactory
                    .getSocketFactory(), 80));
            registry.register(new Scheme("https", sf, 443));

            ClientConnectionManager ccm = new ThreadSafeClientConnManager(
                    params, registry);

            return new DefaultHttpClient(ccm, params);
        } catch (Exception e) {
            return new DefaultHttpClient();
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        progDialog = new ProgressDialog(localContext);
        progDialog.setCancelable(false);
        progDialog.setMessage(msg);
        progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progDialog.show();

    }

    public ArrayList<String> sendPostData(String string,
                                          String result) {
        String URLString = string + "/" + result;
        try {

            AbstractHttpClient httpClient = getNewHttpClient();
            HttpGet httpPost = new HttpGet(URLString);
            httpPost.addHeader(HTTP.CONTENT_TYPE, "text/html");

            HttpResponse httpResponse = httpClient.execute(httpPost);
            HttpEntity entity = httpResponse.getEntity();
            ArrayList<String> localString = new ArrayList<String>();
            String convertedString = EntityUtils.toString(entity, "UTF-8");
            localString.add(convertedString);
            return localString;

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected ArrayList<String> doInBackground(String... params) {

        ArrayList<String> result = new ArrayList<String>();
        String localString = params[0] + "," + params[1];
        return sendPostData("https://api.forecast.io/forecast/b3773c872bd211754afd57928f0ec9df", localString);
    }

    @Override
    public void onPostExecute(ArrayList<String> result) {
        super.onPostExecute(result);
        callBackListener.Oncomplete(result);
        progDialog.dismiss();
    }


}
